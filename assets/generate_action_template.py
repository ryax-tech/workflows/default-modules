import argparse
from pathlib import Path

import yaml


def dir_path(path_to_dir: str):
    dir_arg = Path(path_to_dir)
    if dir_arg.is_dir():
        return dir_arg
    else:
        raise NotADirectoryError(path_to_dir)


def write_ryax_metadata(dir_path: Path) -> None:
    with open(dir_path / "ryax_metadata.yaml", "w") as f:
        yaml.dump(
            {
                "apiVersion": "ryax.tech/v2.0",
                "kind": "Processor",
                "spec": {
                    "id": "CHANGEME",
                    "human_name": "CHANGEME",
                    "type": "python3",
                    "version": "1.0",
                    "description": "CHANGEME",
                    "inputs": [
                        {
                            "help": "CHANGEME",
                            "human_name": "CHANGEME",
                            "name": "CHANGEME",
                            "type": "CHANGEME",
                        }
                    ],
                    "outputs": [
                        {
                            "help": "CHANGEME",
                            "human_name": "CHANGEME",
                            "name": "CHANGEME",
                            "type": "ChANGEME",
                        }
                    ],
                },
            },
            f,
        )


def write_ryax_handler(dir_path: Path) -> None:
    with open(dir_path / "ryax_handler.py", "w") as f:
        f.write("def handle(inputs: dict)->dict:\n\treturn {}")


def write_requirements(dir_path: Path) -> None:
    Path(dir_path / "requirements.txt").touch()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dir_path", type=dir_path)
    args = parser.parse_args()
    write_ryax_metadata(args.dir_path)
    write_ryax_handler(args.dir_path)
    write_requirements(args.dir_path)
