#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import boto3
import json


def handle(req):
    endpoint: str = req.get("s3_endpoint")
    access_key: str = req.get("s3_key")
    secret_key: str = req.get("s3_secret")
    bucket_name: str = req.get("s3_bucket")
    region: str = req.get("s3_region")

    print("Connecting to S3...")
    s3 = boto3.resource(
        endpoint_url=endpoint,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        service_name="s3",
        region_name=region,
    )
    print(f"Listing objects in bucket {bucket_name}...")

    # for b in s3.buckets.all():
    #     print("  "+b.name)

    my_bucket = s3.Bucket(bucket_name)
    print("Listing objects...")
    result_list = []
    for my_bucket_object in my_bucket.objects.all():
        result_list.append(my_bucket_object.key)
        print("  " + my_bucket_object.key)
    result_str = json.dumps(result_list)
    print(result_str)
    return {"s3_files_list": result_str}


if __name__ == "__main__":
    with open("secrets", "r") as secrets_file:
        handle(json.load(secrets_file))
