import OpenWeatherAPI from "openweather-api-node";
import deasync from "deasync";

function handle(ryax_input) {
    let apiKey = ryax_input["apiKey"]
    let city = ryax_input["city"]
    let units = ryax_input["units"]

    console.log(`Inquiring weather for city ${city}...`)

    let weather_config = {
        key: apiKey,
        locationName: city,
        units: units
    }

    let weather = new OpenWeatherAPI(weather_config)

    let done = false
    let weather_output = {}

    weather.getCurrent()
      .then(data => {
        console.log(data);
        if (weather_config.units == "metric") {
          console.log(`Current temperature in ${city} is ${data.weather.temp.cur}\u00B0C`);
        } else if (weather_config.units == "imperial") {
          console.log(`Current temperature in ${city} is ${data.weather.temp.cur}\u00B0F`);
        }
        weather_output = data
        done = true
      })
      .catch(error => {throw new Error(`Error while accessing the API: ${error}`)})
    deasync.loopWhile(function(){return !done;});
    return { "weather_json" : JSON.stringify(weather_output) }
}

export default handle;
