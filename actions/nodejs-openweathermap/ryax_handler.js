import OpenWeatherMapHelper from "openweathermap-node";
import deasync from "deasync";

function handle(ryax_input) {
    let apiKey = ryax_input["apiKey"]
    let units = ryax_input["units"]

    let geo_location = JSON.parse(ryax_input["geo_location"])
    let latitude = geo_location.lat
    let longitude = geo_location.long

    console.log(`Inquiring weather for latitude ${latitude} and longitude ${longitude}...`)

    let weather_config = {
        APPID: apiKey,
        units: units,
        lang: "en"
    }

    let helper = new OpenWeatherMapHelper(weather_config)

    let done = false
    let weather_output = {}

    helper.getCurrentWeatherByGeoCoordinates(latitude, longitude, (err, data) => {
        console.log(data);
        if (weather_config.units == "metric") {
          console.log(`Current temperature in (${longitude}, ${latitude}) is ${data.main.temp}\u00B0C`);
        } else if (weather_config.units == "imperial") {
          console.log(`Current temperature in (${longitude}, ${latitude}) is ${data.main.temp}\u00B0F`);
        }
        weather_output = data
        done = true
    })
    deasync.loopWhile(function(){return !done;});
    return { "weather_json" : JSON.stringify(weather_output) }
}

export default handle;
