import os
import sys
import time
from glob import glob
from operator import add
from random import random

# Get spark home from the Nix store
os.environ["SPARK_HOME"] = glob("/nix/store/*-spark-*/lib/spark-*")[0]
SPARK_HOME = os.environ["SPARK_HOME"]
print(f"SPARK_HOME is {SPARK_HOME}")

# Fix Pyspark python version
os.environ["PYSPARK_PYTHON"] = sys.executable
os.environ["PYSPARK_DRIVER_PYTHON"] = sys.executable
sys.path.append(SPARK_HOME + "/python")

from pyspark import SparkContext  # noqa: E402
from pyspark.conf import SparkConf  # noqa: E402


def f(_):
    x = random() * 2 - 1
    y = random() * 2 - 1
    return 1 if x**2 + y**2 <= 1 else 0


def handle(inputs):
    """
    This code is an adaptation of the official pyspark example
    """
    print("Dumping environment...")
    for key in os.environ.keys():
        print(f"  {key} ===> {os.environ[key]}")

    # default values assure running as a regular ryax action
    ryax_rank = 0
    ryax_size = 1

    try:
        ryax_rank = int(os.environ["SLURM_PROCID"])
        ryax_size = int(os.environ["SLURM_NPROCS"])
    except KeyError:
        pass

    partitions = int(inputs["partitions"])
    threads = 4
    output_file = f"/tmp/computedpi-{ryax_rank}-of-{ryax_size-1}.txt"
    conf = SparkConf()
    sc = SparkContext(master=f"local[{threads}]", appName="PythonPi", conf=conf)
    print(f"Rank {ryax_rank} : Partitions = {partitions}")
    n = int((10000 * partitions) / ryax_size)
    if ryax_rank == 0:
        n += (10000 * partitions) % ryax_size

    print(f"Rank {ryax_rank} : Number of points to generate N = {n}")
    count = sc.parallelize(range(1, n + 1), partitions).map(f).reduce(add)
    sc.stop()
    print(f"Rank {ryax_rank} : Found {count} points inside the quarter of circle")

    retries = 10000
    retry_interval = 2
    output_filepath = "/iodir/outputs"

    # If it is the coordinator process, SLURM_PROCID == 0
    if ryax_rank == 0:
        if ryax_size > 1:
            # wait until the number of files in /iodir/outputs == ryax_size-1
            while len(os.listdir(output_filepath)) < ryax_size - 1 and retries > 0:
                print(
                    f"Rank {ryax_rank} : Waiting for result files, have {len(os.listdir(output_filepath))} out of {ryax_size}"
                )
                retries -= 1
                time.sleep(retry_interval)
                print(f"Rank {ryax_rank} : Retrying  {retries}")

            output_files = os.listdir(output_filepath)
            print(f"Rank {ryax_rank} : Found {len(output_files)}")

            for path in output_files:
                output_filename = os.path.join(output_filepath, path)
                print(f"Rank {ryax_rank} : Reading output file {output_filename}")
                with open(output_filename, "r") as count_file:
                    count += int(count_file.read())

        # read all files count
        pi = (4.0 * count) / (10000 * partitions)
        print(f"Rank {ryax_rank} : Pi is roughly %f" % pi)
        output_file = "/tmp/final_output.txt"
        with open(output_file, "w") as pi_output:
            pi_output.write(str(pi))
        return {"pi": pi, "sparks_result_file": output_file}
    # regular process will just write the count of points on a result file
    else:
        with open(output_file, "w") as pi_output:
            pi_output.write(str(count))
        return {"pi": "NA", "sparks_result_file": output_file}
