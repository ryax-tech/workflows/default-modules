#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json

import psycopg2


def load_json(where: str) -> None:
    with open(where, "r") as f:
        return json.load(f)


def handle(module_input):
    name = module_input["name"]
    host = module_input["host"]
    username = module_input["username"]
    password = module_input["password"]
    port = module_input["port"]
    jsonfile = module_input["insertdata"]
    table_name = module_input["tablename"]

    try:
        engine = psycopg2.connect(
            database=name, user=username, password=password, host=host, port=port
        )
        cursor = engine.cursor()
        json_content = load_json(jsonfile)
        if len(json_content) == 0:
            raise Exception("The json to upload to the DB was empty. Aborting.")

        jsonlist = (
            [list(x.values()) for x in json_content]
            if isinstance(json_content[0], dict)
            else json_content
        )

        # Search for null values
        removed_nulls = []
        for entry in jsonlist:
            new_entry = []
            for data_point in entry:
                if data_point is None:
                    new_entry.append("NULL")
                else:
                    new_entry.append(data_point)
            removed_nulls.append(new_entry)

        content = [tuple(x) for x in removed_nulls]
        content_str = f"{content}"
        content_query = content_str[1:-1]

        query: str = f"INSERT into {table_name} values {content_query}"
        cursor.execute(query)
        engine.commit()
        engine.close()

    except Exception as err:
        print(f"An error occurred while attempting this transaction\n{err}")
