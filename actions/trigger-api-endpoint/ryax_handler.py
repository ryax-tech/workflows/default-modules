#!/usr/bin/env python3

import json

import requests


def handle(inputs: dict) -> dict:
    parameters = json.loads(inputs["parameters"])
    resp = requests.post(
        inputs["endpoint"],
        headers={"content-type": "application/json"},
        verify=False,
        params=parameters,
    )
    result = resp.json()
    print(
        f"Query:\n\tPOST {inputs['endpoint']} with parameters {inputs['parameters']}\n\nResult:\n\t{resp} - {result}"
    )
    return {}
