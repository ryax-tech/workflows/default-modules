#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import datetime
from pathlib import Path

import boto3
from botocore.config import Config

def create_bucket_key(file_name, timestamp: bool):
    file_name = Path(file_name)
    if timestamp:
        date = datetime.datetime.now().isoformat()
        return file_name.stem + "_" + date + file_name.suffix
    return file_name.stem + file_name.suffix


def connect_to_bucket(name: str, access_key: str, secret_key: str):
    s3 = boto3.resource(
        "s3", aws_access_key_id=access_key, aws_secret_access_key=secret_key
    )
    return s3.Bucket(name)


def handle(req):
    endpoint: str = req.get("s3_endpoint")
    access_key: str = req.get("s3_key")
    secret_key: str = req.get("s3_secret")
    bucket_name: str = req.get("s3_bucket")
    region: str = req.get("s3_region")
    file_path: str = req.get("file_to_upload")
    object_path: str = req.get("object_path", "")

    print("Connecting to S3...")
    s3 = boto3.resource(
        endpoint_url=endpoint,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        service_name="s3",
        region_name=region,
        config=Config(
            request_checksum_calculation="when_required",
            response_checksum_validation="when_required",
        )
    )

    print(f"Timestamp value '{req.get('timestamp')}'")

    if req.get("timestamp") is None:
        timestamp_bool = False
    elif str(req.get("timestamp")).lower() == "false" or str(req.get("timestamp")) == 0:
        timestamp_bool = False
    else:
        timestamp_bool = True

    my_bucket = s3.Bucket(bucket_name)
    object_path += create_bucket_key(file_path, timestamp_bool)
    print(f"Uploading file {file_path} to {object_path}")
    if not Path(file_path).exists():
        print(f"Error the file to upload: {file_path} does not exists!")
        exit(1)
    my_bucket.upload_file(file_path, object_path)
    return {}


if __name__ == "__main__":
    import json

    with open("secrets", "r") as secrets_file:
        json_input = json.load(secrets_file)

    import sys
    if len(sys.argv) >= 2:
        json_input["file_to_upload"] = sys.argv[1]
    else:
        json_input["file_to_upload"] = "requirements.txt"
    if len(sys.argv) == 3:
        json_input["object_path"] = sys.argv[2]

    json_input["timestamp"] = "False"
    handle(json_input)
