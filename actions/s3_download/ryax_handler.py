#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
from pathlib import Path

import boto3
import botocore
import sys


def handle(req):
    endpoint: str = req.get("s3_endpoint")
    access_key: str = req.get("s3_key")
    secret_key: str = req.get("s3_secret")
    bucket_name: str = req.get("s3_bucket")
    region: str = req.get("s3_region")
    object_key: str = req.get("s3_object")

    print("Connecting to S3...")
    s3 = boto3.resource(
        endpoint_url=endpoint,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        service_name="s3",
        region_name=region,
    )
    print(f"Listing objects in bucket {bucket_name}...")

    my_bucket = s3.Bucket(bucket_name)

    local_filename = Path(f"/tmp/{Path(object_key).name}")

    print(f"Downloading object {object_key} to {local_filename}...")
    try:
        my_bucket.download_file(object_key, local_filename)
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "404":
            print("The object does not exist.")
        else:
            raise

    return {"s3_file": str(local_filename)}


if __name__ == "__main__":
    import json

    with open("secrets", "r") as secrets_file:
        json_input = json.load(secrets_file)

    print("Calling with object name : " + sys.argv[1])
    json_input["s3_object"] = sys.argv[1]
    ret_json = handle(json_input)
    import json
    print(json.dumps(ret_json, indent=4))