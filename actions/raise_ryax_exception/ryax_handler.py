#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
from dataclasses import dataclass


@dataclass
class RyaxException(Exception):
    message: str
    code: int


def handle(mod_in: dict):
    code = mod_in["code"]
    message = mod_in["message"]

    raise RyaxException(code=code, message=message)
