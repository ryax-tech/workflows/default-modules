#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json

import psycopg2


def load_json(where: str) -> None:
    with open(where, "r") as f:
        return json.load(f)


def handle(module_input):
    name = module_input["name"]
    host = module_input["host"]
    username = module_input["username"]
    password = module_input["password"]
    port = module_input["port"]
    jsonfile = module_input["insertdata"]
    table_name = module_input["tablename"]
    pk = module_input["pk"]

    try:
        engine = psycopg2.connect(
            database=name, user=username, password=password, host=host, port=port
        )
        cursor = engine.cursor()
        json_content = load_json(jsonfile)
        if len(json_content) == 0:
            raise Exception("The json to upload to the DB was empty. Aborting.")

        jsonlist = (
            [list(x.values()) for x in json_content]
            if isinstance(json_content[0], dict)
            else json_content
        )

        # Search for null values
        removed_nulls = []
        for entry in jsonlist:
            new_entry = []
            for data_point in entry:
                if data_point is None:
                    new_entry.append("NULL")
                else:
                    new_entry.append(data_point)
            removed_nulls.append(new_entry)

        cursor.execute(f"select * from {table_name}")
        _ = cursor.fetchone()
        column_names: list = [column_info[0] for column_info in cursor.description]
        for entry in removed_nulls:
            if len(entry) != len(column_names):
                raise Exception(
                    f"Row {entry} does not have matching amount of columns to the database coumns: {column_names}"
                )

            value_update_statement = f"UPDATE {table_name} SET "
            for column_name, cont in zip(column_names, entry):
                put_cont = f"'{cont}'" if isinstance(cont, str) else cont
                value_update_statement += f"{str(column_name)}={put_cont},"
            if value_update_statement.endswith(","):
                value_update_statement = value_update_statement[:-1]
            value_update_statement += f" WHERE {pk} = '{entry[0]}'"
            print(f"Executing update: {value_update_statement}")
            cursor.execute(value_update_statement)
            engine.commit()
        print("DONE")
        engine.close()

    except Exception as err:
        print(f"An error occurred while attempting this transaction\n{err}")
