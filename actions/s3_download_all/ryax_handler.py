#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


from pathlib import Path

import boto3


def connect_to_bucket(name: str, access_key: str, secret_key: str):
    s3 = boto3.resource(
        "s3", aws_access_key_id=access_key, aws_secret_access_key=secret_key
    )
    return s3.Bucket(name)


def handle(req):
    access_key: str = req.get("key_id")
    secret_key: str = req.get("secret_key_id")
    bucket_name: str = req.get("bucket_name")

    my_bucket = connect_to_bucket(bucket_name, access_key, secret_key)
    all_objs = sorted([obj.key for obj in my_bucket.objects.all()])

    download_loc = Path("/tmp/downloaded_bucket_files")
    download_loc.mkdir(exist_ok=False)

    for obj in all_objs:
        my_bucket.download_file(obj.key, str(download_loc) + "/" + str(obj.key))
    return {"output_data": download_loc}
