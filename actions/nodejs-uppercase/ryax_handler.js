var uc = require('upper-case');

function handle(ryax_input) {
    output_str = uc.upperCase(ryax_input["input_str"]);
    return { "output_str" : output_str };
}
module.exports = handle;