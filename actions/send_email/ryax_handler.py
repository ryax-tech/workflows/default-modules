#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import smtplib
import ssl
from email.message import EmailMessage


def handle(req):
    message = EmailMessage()
    message.set_content(req["message"])
    message["From"] = req["from"]
    message["To"] = req["to"]
    message["Subject"] = req["subject"]
    context = ssl.create_default_context()
    with smtplib.SMTP(req["host"], req["port"]) as smtp:
        smtp.starttls(context=context)
        smtp.login(req["from"], req["password"])
        smtp.send_message(message)
