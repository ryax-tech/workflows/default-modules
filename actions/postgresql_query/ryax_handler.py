#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json

import psycopg2


def save_json(obj: list, where: str) -> None:
    with open(where, "w") as f:
        return json.dump(obj, f)


def handle(module_input):
    name = module_input["name"]
    host = module_input["host"]
    username = module_input["username"]
    password = module_input["password"]
    query = module_input["query"]
    port = module_input["port"]

    save_location = "/tmp/query_result.json"

    try:
        engine = psycopg2.connect(
            database=name, user=username, password=password, host=host, port=port
        )
        cursor = engine.cursor()
        cursor.execute(query)
        column_names: list = [column_info[0] for column_info in cursor.description]
        rows = cursor.fetchall()

        assert rows, "No resulting data was found by this query."
        assert len(rows[0]) == len(
            column_names
        ), "The data retrieved does not correspond to the column information."
        formatted_rows = []
        for row in rows:
            row_dict = {}
            for idx, colname in enumerate(column_names):
                val = row[idx] if row[idx] != "NULL" else None
                row_dict[colname] = val
            print(json.dumps(row_dict, indent=2))
            formatted_rows.append(row_dict)

        save_json(formatted_rows, save_location)

    except Exception as err:
        print(f"An error occurred while attempting this transaction\n{err}")
        return {}

    print("SUCCESS")

    return {"result": save_location}
