#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import datetime
from typing import Dict

from ryax_execution.ryax_trigger_protocol import RyaxTriggerProtocol


class InvalidTimeException(Exception):
    pass


def emit_time():
    return datetime.datetime.now().isoformat()


def time_to_seconds(input_values: dict) -> int:
    times_seconds = {"days": 86400, "hours": 3600, "minutes": 60, "seconds": 1}
    total_time = 0.0
    for unit, amount in input_values.items():
        if not amount:
            continue
        total_time += times_seconds.get(unit, 0) * amount
    return total_time


async def run(service: RyaxTriggerProtocol, input_values: Dict):
    if all(v is None for v in input_values.values()):
        print("At least one unit of time must be specified")
        raise InvalidTimeException
    total_time = time_to_seconds(input_values)
    while True:
        await service.create_run({"time": emit_time()})
        print(f"Waiting for the next execution in {total_time} seconds")
        await asyncio.sleep(total_time)
        print("Trigger a new execution!")
