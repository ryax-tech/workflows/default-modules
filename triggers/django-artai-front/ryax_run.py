import asyncio
import os

"""
This action was made having a stable diffusion API to reach for generating an art image.
"""


async def run(service, input_values: dict) -> None:
    """Run administrative tasks."""
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ryaxdjangofront.settings")
    os.environ["RYAX_INSTANCE_NAME"] = input_values.get("ryax_endpoint_instance_name")
    ryax_prefix = input_values.get("ryax_endpoint_prefix")
    if ryax_prefix != "":
        if ryax_prefix[0] == "/":
            ryax_prefix = ryax_prefix[1:-1] + ryax_prefix[-1]
        if ryax_prefix[-1] != "/":
            ryax_prefix += "/"
    print(f"Setting up URLS with prefix ===>{ryax_prefix}<===")
    os.environ["RYAX_PREFIX"] = ryax_prefix
    os.environ["REDIRECT_PREFIX"] = (
        input_values.get("ryax_endpoint_protocol")
        + "://"
        + input_values.get("ryax_endpoint_instance_name")
        + "/"
        + ryax_prefix
    )
    print(f"REDIRECT_PREFIX ==>{os.environ['REDIRECT_PREFIX']}<==")
    os.environ["REPLICATE_URL"] = input_values.get("replicate_url")
    os.environ["REPLICATE_TOKEN"] = input_values.get("replicate_token")
    os.environ["RYAX_ARTAI_URL"] = input_values.get("ryax_artai_url")
    proc = await asyncio.subprocess.create_subprocess_shell(
        f"python manage.py runserver 0.0.0.0:{input_values.get('ryax_endpoint_port')}"
    )
    await proc.wait()


if __name__ == "__main__":
    input_json = {
        "ryax_endpoint_protocol": "http",
        "ryax_endpoint_instance_name": "localhost",
        "ryax_endpoint_prefix": "/artai",
        "ryax_endpoint_port": 8080,
        "replicate_url": "secret",
        "replicate_token": "secret",
        "ryax_artai_url": "https://myapi.com/stablediffusion/input",
    }

    # Populate with secrets for testing
    import json

    with open("secrets.txt") as f:
        secrets = json.load(f)
        for key in secrets:
            if key in input_json:
                input_json[key] = secrets[key]

    from unittest.mock import AsyncMock

    asyncio.run(run(AsyncMock(), input_json))
