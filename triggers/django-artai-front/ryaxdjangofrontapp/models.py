from django.db import models


class artai_prompt(models.Model):
    # addressees full legal name
    prompt = models.CharField(max_length=1000, blank=False)
