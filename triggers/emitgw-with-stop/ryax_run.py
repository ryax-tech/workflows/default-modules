#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import datetime
from typing import Dict

from ryax_execution.ryax_trigger_protocol import RyaxTriggerProtocol


class InvalidTimeException(Exception):
    pass


def emit_time():
    return datetime.datetime.now().isoformat()


def time_to_seconds(input_values: dict) -> int:
    times_seconds = {"days": 86400, "hours": 3600, "minutes": 60, "seconds": 1}
    total_time = 0.0

    for unit, amount in input_values.items():
        if not amount:
            continue
        if amount < 0:
            print("Values must be positive")
            raise InvalidTimeException
        total_time += times_seconds.get(unit, 0) * amount

    return total_time


async def run(service: RyaxTriggerProtocol, input_values: Dict):
    total_time = time_to_seconds(input_values)
    if total_time == 0.0:
        print("At least one unit of time must be specified and non-zero")
        raise InvalidTimeException

    iteration = 0
    timer = 0
    iteration_limited_reached = False
    time_limited_reached = False
    while True:
        timer += total_time  # timer accounts for the next execution in the future
        print(f"Checking if {timer}s exceeds limit {input_values.get('time_limit')}")
        if input_values.get("time_limit") is not None:
            if timer > int(input_values["time_limit"]):
                print(f"Reached the time limit {input_values['time_limit']}")
                time_limited_reached = True

        iteration += 1
        if input_values.get("iterations_limit") is not None:
            print(
                f"Checking if iteration {iteration} exceeds limit {input_values.get('iterations_limit')}"
            )
            if iteration > input_values["iterations_limit"]:
                print(
                    f"Reached the iterations limit {input_values['iterations_limit']}"
                )
                iteration_limited_reached = True

        if iteration_limited_reached or time_limited_reached:
            print("Execution not started because limit was reached!")
        else:
            print("Trigger a new execution!")
            await service.create_run({"time": emit_time()})

        print(f"Waiting for the next execution in {total_time} seconds")
        await asyncio.sleep(total_time)
