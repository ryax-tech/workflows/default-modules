import asyncio
import pathlib
import shutil

"""
JupyterLab instance running with common lbraries available.
"""


async def run(service, input_values: dict) -> None:
    """Run jupyther notbook server"""
    ryax_port = input_values.get("ryax_endpoint_port")

    ryax_prefix = input_values.get("ryax_endpoint_prefix")
    if ryax_prefix != "":
        if ryax_prefix[0] == "/":
            ryax_prefix = ryax_prefix[1:-1] + ryax_prefix[-1]
        if ryax_prefix[-1] != "/":
            ryax_prefix += "/"
    print(f"Setting up URLS with prefix ===>{ryax_prefix}<===")

    token = input_values.get("token")

    app_dir = "/tmp/jupyter/app"
    shutil.rmtree(app_dir, ignore_errors=True)
    pathlib.Path(app_dir).mkdir(parents=True, exist_ok=True)
    workdir = "/tmp/jupyter/workdir"
    shutil.rmtree(workdir, ignore_errors=True)
    pathlib.Path(workdir).mkdir(parents=True, exist_ok=True)

    print("INITIALIZING...")
    proc = await asyncio.subprocess.create_subprocess_shell(
        f"python3 -m jupyterlab build --app-dir={app_dir}"
    )
    await proc.wait()
    proc = await asyncio.subprocess.create_subprocess_shell(
        "python3 -m venv /tmp/.venv"
    )
    await proc.wait()

    command = (
        "source /tmp/.venv/bin/activate;"
        f"python3 -m jupyterlab --app-dir={app_dir} --notebook-dir={workdir} "
        f"--ip=0.0.0.0 --port={ryax_port} --ServerApp.token='{token}' "
        f"--ServerApp.base_url='{ryax_prefix}'"
    )
    print("COMMAND TO RUN: ")
    print(command)
    proc = await asyncio.subprocess.create_subprocess_shell(command)
    await proc.wait()


if __name__ == "__main__":
    input_json = {
        "ryax_endpoint_prefix": "/notebook",
        "ryax_endpoint_port": 8080,
        "token": "secret",
    }

    from unittest.mock import AsyncMock

    asyncio.run(run(AsyncMock(), input_json))
