#!/usr/bin/env bash

set -e
# For debug
# set -x

while getopts "fd:" opt; do
  case "$opt" in
    f)
      DO_CHECK_ONLY="false"
      ;;
    d)
      CHECK_DIR="$OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-f] [-d directory]" >&2
      exit 1
      ;;
  esac
done

TO_CHECK_DIR=${CHECK_DIR:-"."}
CHECK_ONLY=${DO_CHECK_ONLY:-"true"}
RUFF_EXTRA_OPTS="--fix"

if [[ $CHECK_ONLY == "true" ]]
then
    RUFF_CHECK_EXTRA_OPTS=""
    RUFF_FORMAT_EXTRA_OPTS="--check --diff"
fi

echo "-- Checking python formating"
ruff format $TO_CHECK_DIR --exclude ".venv|.poetry" $RUFF_FORMAT_EXTRA_OPTS

echo "-- Running python static checking"
ruff check $TO_CHECK_DIR $RUFF_CHECK_EXTRA_OPTS


